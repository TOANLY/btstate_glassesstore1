
import './App.css';
import BTState from './State/BtState';


function App() {
  return (
    <div className="App">
      <BTState/>
    </div>
  );
}

export default App;
