import React from "react";

const ProductList = (props) => {
  const { data, setState } = props;
  return (
    <div className="container mt-5">
      <div className="flex-wrap d-flex justify-center p-4 ">
        {data.map((product) => {
          return (
            <div key={product.id} className="p-3">
              <button onClick={() => setState(product)} className="mr-4 ">
                <img
                  style={{ height: 40, width: 100 }}
                  src={product.url}
                  alt="..."
                />
              </button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ProductList;
