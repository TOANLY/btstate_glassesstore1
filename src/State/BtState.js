import React, { useState } from "react";
import Header from "./Header";
import StateBody from "./StateBody";
import data from "./data.json";
import ProductList from "./ProductList";

const BTState = () => {
  const [product, setProduct] = useState(data[0]);
  const changePattern = (product) => {
    setProduct(product);
  };
  return (
    <div className="">
      <Header />
      <StateBody dataProduct={product} />
      <ProductList setState={changePattern} data={data} />
    </div>
  );
};

export default BTState;
