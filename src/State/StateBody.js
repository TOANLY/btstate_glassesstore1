import React from "react";

const StateBody = ({ dataProduct }) => {
  return (
    <div className="container">
      <div className="d-flex justify-content-around mt-5">
        <div style={{ position: "relative" }}>
          <img
            src="./image/model.jpg"
            alt="..."
            className="img-fluid "
            style={{ width: 250, height: 300 }}
          />
          <div
            style={{
              position: "absolute",
              top: 75,
              left: 63,
              mixBlendMode: "multiply",
            }}
          >
            <img
              style={{
                width: 125,
              }}
              src={dataProduct.url}
              
            />
          </div>
          <div
            className=""
            style={{
              textAlign: "left",
              bottom: 0,
              position: "absolute",
              backgroundColor: "#888",
            }}
          >
            <div className="ml-2">
              <span className="text-warning" style={{ fontSize: 20 }}>
                {dataProduct.name}
              </span>
              <p className="text-primary m-0">Price: {dataProduct.price}$</p>
              <p className="text-white m-0">
                <b>{dataProduct.desc}</b>
              </p>
            </div>
          </div>
        </div>
        <div>
          <img
            src="./image/model.jpg"
            alt="..."
            className="img-fluid"
            style={{ width: 250, height: 300 }}
          />
          <div
            style={{
              position: "absolute",
              top: 255,
              left: 1495,
              mixBlendMode: "multiply",
            }}
          >
            <img
              style={{
                width: 125,
              }}
              src={dataProduct.url}
              alt="..."
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default StateBody;
